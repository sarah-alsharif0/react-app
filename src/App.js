import React from "react";
import './App.css'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newItem: "",
      list: []
    };
  }


  updateInput(key, value) {
    this.setState({ [key]: value });
  }

  addItem() {

    const newItem = {
      id: 1 + Math.random(),
      value: this.state.newItem
    };

    const list = [...this.state.list];
    list.push(newItem);

    this.setState({
      list,
      newItem: ""
    });
  }

  deleteItem(id) {
    
    const list = [...this.state.list];
    
    const updatedList = list.filter(item => item.id !== id);

    this.setState({ list: updatedList });
  }
  
  render() {
    return (
      <div>

      <h1 className="app-title">My Tasks</h1>
        
        <div className="container">
        <div className="inner-container">
          Add an Item...
          <br /> <br />
          <input
            className="input-container"
            type="text"
            placeholder="Type item here"
            value={this.state.newItem}
            onChange={e => this.updateInput("newItem", e.target.value)}
          />
          <button
            className="add-btn"
            onClick={() => this.addItem()}
            disabled={!this.state.newItem.length}
          >
            <i class="material-icons"> add </i>
          </button>
          <br /> <br />
          <ol className="tasks-list">
            {this.state.list.map(item => {
              return (
                <li key={item.id}>
                  {item.value}
                  <button className="del-btn" onClick={() => this.deleteItem(item.id)}>
                    <i class="material-icons">delete</i>
                  </button>
                </li>
              );
            })}
          </ol>
        </div>
      </div>
      </div>
    );
  }
}


export default App;